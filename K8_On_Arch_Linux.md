# Arch Linux hosting Kubernetes using libvirt
* [https://wiki.archlinux.org/title/Libvirt](https://wiki.archlinux.org/title/Libvirt)
* [https://wiki.archlinux.org/title/QEMU#Enabling_KVM](https://wiki.archlinux.org/title/QEMU#Enabling_KVM)
* [https://wiki.archlinux.org/title/KVM](https://wiki.archlinux.org/title/KVM)

Check host to verify kvm will function, see offiocial arch linux wiki for full details in the URL above.

```bash
LC_ALL=C lscpu | grep Virtualization
zgrep VIRTIO /proc/config.gz
lsmod | grep kvm
```

Install libvirt and virt-manager to handle cli and gui management of virtual machines hosting kubernetes.

```bash
sudo pacman -Sy libvirt virt-manager
```
Check what [groups](https://wiki.archlinux.org/title/Users_and_groups) your user account is in, we need to add ourselves to groups `libvirt` and `kvm`. In these groups do not exist you will first need to create the group and then add your `$USER` to it.

```bash
groups $USER
```
Create group
```bash
groupadd kvm
```
Add user to group
```bash
gpasswd -a $USER kvm
```
Or add username manually by editing the `/etc/group` file.

Now check your $USER account and verify you are part of `libvirt` and `kvm`

```bash
id $USER
``` 
# Prepping CoreOS for libvirt build on Arch Linux

* [https://docs.fedoraproject.org/en-US/fedora-coreos/provisioning-libvirt/](https://docs.fedoraproject.org/en-US/fedora-coreos/provisioning-libvirt/)
* [https://docs.fedoraproject.org/en-US/fedora-coreos/producing-ign/](https://docs.fedoraproject.org/en-US/fedora-coreos/producing-ign/)

Create a directory where you will edit, download and store all your items for this project:

```bash
mkdir K8_private_build
```
Inside this directory you will need to create cusom YAML files for your Coreos build. You can customize each YAML file or use the same for all depending on how much difference you need between servers. Save the YAML file as a Butane file `controlplane.bu` and convert it to a Ignition file `controlplane.ign` using [Butane](https://github.com/coreos/butane/blob/main/docs/getting-started.md).
 
```bash
variant: fcos
version: 1.1.0
passwd:
  users:
    - name: core
      groups:
        - docker
        - wheel
        - sudo
      password_hash: XXXXXXX
      ssh_authorized_keys:
        - ssh-rsa XXXXXXXX= dude@dudescomputer
storage:
  files:
    - path: /etc/sysctl.d/20-silence-audit.conf
      contents:
        inline: |
          kernel.printk=4
    - path: /etc/hostname
      mode: 420
      contents:
        source: "data:,fcos"
    - path: /etc/NetworkManager/system-connections/enp1s0.nmconnection
      mode: 0600
      overwrite: true
      contents:
        inline: |
          [connection]
          type=ethernet
          id=network1
          interface-name=ens3

          [ipv4]
          method=manual
          addresses=192.168.99.100/24
          gateway=192.168.99.1
          dns=192.168.99.1;8.8.8.8
```
Use the following Python command to generate a proper password and add it to your YAML file shown above.
```python
python -c "import crypt,random,string; print(crypt.crypt(input('clear-text password: '), '\$6\$' + ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(16)])))"
```
Use your existing or create a special public ssh key and add it to the `ssh-rsa` shown in the above YAML file.

Install docker or podman for running containers locally for now.
* [https://wiki.archlinux.org/title/Podman](https://wiki.archlinux.org/title/Podman)
* [https://github.com/coreos/butane/releases](https://github.com/coreos/butane/releases)
```
sudo pacman -Sy podman
```
Compile butane file, aka YAML file, into an ignition file.

```bash
podman run --interactive --rm quay.io/coreos/butane:release \
--pretty --strict < controlplane.bu > controlplane.ign
```
View existing Architectures available.
```bash
podman run \
--rm \
--tty \
--interactive \
--security-opt \
label=disable \
--volume $HOME/my/custom/path/K8_private_build/:/pwd \
--workdir /pwd quay.io/coreos/coreos-installer:release list-stream
```
Use the qcow2

```bash
podman run --pull=always --rm -v $HOME/my/custom/path/K8_private_build/:/data -w /data \
quay.io/coreos/coreos-installer:release download -s "${STREAM}" -p qemu -f qcow2.xz --decompress
```
## Building CoreOS with libVirt tools
* [https://wiki.libvirt.org/page/Networking](https://wiki.libvirt.org/page/Networking)
* [https://libvirt.org/docs.html](https://libvirt.org/docs.html)

### Quick vish reference

List all virsh domains

```bash
virsh list --all
```
View all network leases on default network
```bash
virsh net-dhcp-leases default
```
Check domain network
```bash
virsh domifaddr --source agent nodeAlpha
```
Shutdown and remove libvirt domain

```bash
virsh destroy --domain nodeAlpha
virsh undefine --domain nodeAlpha
```

Create the first coreos VM which will be used for the Kubernetes Control Plane. Run using `sudo` so at to have access to the full network and not run in the limited `usermode`.
```bash
sudo virt-install --name=cpVM \
        --vcpus=2 \
        --ram=2048 \
        --os-variant=fedora6 \
        --import \
        --network=default \
        --graphics=none \
        --disk=size=20,backing_store=$HOME/my/custom/path/K8_private_build/fedora-coreos-35.20211203.3.0-qemu.x86_64.qcow2 \
        --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=$HOME/my/custom/path/K8_private_build/controlplane.ign"
```

View host and virsh networking details within [fedoraCoreos](https://gitlab.com/trentonknight/kubernetes_test_scripts/-/blob/main/fedoraCoreos.md). 

The first Node which will be used to connect to the Control Plane later, `NodeAlpha`:

```bash
variant: fcos
version: 1.1.0
passwd:
  users:
    - name: core
      groups:
        - docker
        - wheel
        - sudo
      password_hash: XXXXXXXXXX
      ssh_authorized_keys:
        - ssh-rsa XXXXXXXXXXXXX 
storage:
  files:
    - path: /etc/sysctl.d/20-silence-audit.conf
      contents:
        inline: |
          kernel.printk=4
    - path: /etc/hostname
      mode: 420
      contents:
        source: "data:,fcos"
    - path: /etc/NetworkManager/system-connections/ens2.nmconnection
      mode: 0600
      overwrite: true
      contents:
        inline: |
          [connection]
          id=ens2
          type=ethernet
          interface-name=ens2
          [ipv4]
          address1=10.0.2.16/24,10.0.2.2
          dhcp-hostname=nodeAlpha
          dns=8.8.8.8
          dns-search=
          may-fail=false
          method=manual
```
Write your `kubeadm-config.yaml` for initing a new cluster.

```bash
cat << EOF > kubeadm-config.yaml
apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
nodeRegistration:
  kubeletExtraArgs:
    volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"
localAPIEndpoint:
  advertiseAddress: "10.10.99.101"
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  podSubnet: "10.10.0.0/16"
controllerManager:
  extraArgs:
    flex-volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"
apiServer:
  extraArgs:
    advertise-address: 10.10.99.101

EOF
```
```bash
sudo kubeadm init --config kubeadm-config.yaml
```

Compile butane file, aka YAML file, into an ignition file.

```bash
podman run --interactive --rm quay.io/coreos/butane:release \
--pretty --strict < nodeAlpha.yaml > nodeAlpha.ign
```

```bash
virt-install --name=nodeAlpha \
        --vcpus=2 \
        --ram=2048 \
        --os-variant=fedora6 \
        --import \
        --network=default \
        --graphics=none \
        --disk=size=20,backing_store=$HOME/Development/K8_private_build/fedora-coreos-35.20211203.3.0-qemu.x86_64.qcow2 \
        --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=$HOME/Development/K8_private_build/nodeAlpha.ign"
```

Compile butane file, aka YAML file, into an ignition file.

```bash
podman run --interactive --rm quay.io/coreos/butane:release \
--pretty --strict < nodeBravo.yaml > nodeBravo.ign
```

```bash
virt-install --name=nodeBravo \
        --vcpus=2 \
        --ram=2048 \
        --os-variant=fedora6 \
        --import \
        --network=default \
        --graphics=none \
        --disk=size=20,backing_store=$HOME/Development/K8_private_build/fedora-coreos-35.20211203.3.0-qemu.x86_64.qcow2 \
        --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=$HOME/Development/K8_private_build/nodeBravo.ign"
```
