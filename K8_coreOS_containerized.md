# Guide for installing custom Kubernetes K8 using linux conatiners running coreos.


* [https://coreos.github.io/butane/specs/](https://coreos.github.io/butane/specs/)

```bash
sudo podman run --pull=always --privileged --rm \
    -v /dev:/dev -v /run/udev:/run/udev -v .:/data -w /data \
    quay.io/coreos/coreos-installer:release \
    install /dev/vdb -i config.ign
```
