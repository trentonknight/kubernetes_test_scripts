# Kind cheatsheet for Kubernetes clusters on Arch Linux

* [https://kind.sigs.k8s.io/](https://kind.sigs.k8s.io/)

## Install Go, Docker, Kind 

### Docker install
```bash
sudo pacman -Sy docker
sudo enable docker
sudo systemctl enable docker
sudo systemctl start docker
```
### Go Install 

```bash
tar -C /usr/local -xzf go1.17.5.linux-amd64.tar.gz
```
Append to `.bashrc`

```bash
export PATH=$PATH:/usr/local/go/bin
```
Test to verify it works

```bash
go version
```
### Kind Install

* [https://kind.sigs.k8s.io/docs/user/quick-start/#installation](https://kind.sigs.k8s.io/docs/user/quick-start/#installation)

```bash
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64
chmod +x ./kind
sudo mv kind /usr/bin
```

### Kubectl Install

* [https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

* [https://kubernetes.io/docs/reference/kubectl/cheatsheet/](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

* [https://kubectl.docs.kubernetes.io/](https://kubectl.docs.kubernetes.io/)

Kubectl binary

```bash
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
```

Kubectl Checksum

```bash
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
```

Validate the kubectl binary against the checksum file

```bash
echo "$(<kubectl.sha256)  kubectl" | sha256sum --check
```
Install Kubectl

```bash
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```
Test Kubectl

```bash
kubectl version --client
```

# Create a multi-node cluster with Kind 

* [https://kind.sigs.k8s.io/docs/user/configuration/#nodes](https://kind.sigs.k8s.io/docs/user/configuration/#nodes)

Create a control plane and two nodes for running pods using a Yaml file as follows:

```bash
vim kind-example-config.yaml
```
One very basic example which will do the job.

```yaml
# three node (two workers) cluster config
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: worker
- role: worker
```

```bash
sudo kind create cluster --config kind-example-config.yaml
```


## Kind CLI

* [https://kind.sigs.k8s.io/docs/user/quick-start/#creating-a-cluster](https://kind.sigs.k8s.io/docs/user/quick-start/#creating-a-cluster)

A few Kind commands

Review clusters created, assuming cluster name below is `kind` and not something else. Regardless the get cluster command will show you.

```bash
sudo kind get clusters
sudo kubectl cluster-info --context kind-kind
```
Delete unwanted clusters

```bash
sudo kind delete cluster
```
You can get a list of images present on a cluster node by using `docker exec`:

```bash
docker exec -it my-node-name crictl images
```
Get Logs

```bash
mkdir logs
sudo kind export logs logs/
```
### Kindnet CNI

Kind uses [kindnet](https://github.com/aojea/kindnet#kindnet-components) for a CNI. You can view default installed pods using `kubectl get pods` as follows:

```bash
sudo kubectl get pods -n kube-system
``` 
## Accessing control plane directly for Kubeadm commands

Find the control plane `CONTAINER ID` using `docker ps`. For example:

```bash
[trentonknight@archMac logs]$ sudo docker ps
CONTAINER ID   IMAGE                  COMMAND                  CREATED       STATUS       PORTS                       NAMES
5ef06df59607   kindest/node:v1.21.1   "/usr/local/bin/entr…"   2 hours ago   Up 2 hours                               kind-worker
2e0cb7a7834d   kindest/node:v1.21.1   "/usr/local/bin/entr…"   2 hours ago   Up 2 hours   127.0.0.1:45857->6443/tcp   kind-control-plane
b47b0cadd340   kindest/node:v1.21.1   "/usr/local/bin/entr…"   2 hours ago   Up 2 hours                               kind-worker2
```
Grab a Bash shell on the control plane discovered

```bash
sudo docker exec -it 2e0cb7a7834d bash
```
Test for kubeadm install

```bash
kubeadm
```

## Kubeadm to check authentication

* [https://kubernetes.io/docs/reference/access-authn-authz/authentication/](https://kubernetes.io/docs/reference/access-authn-authz/authentication/)

* [https://kubernetes.io/docs/setup/best-practices/certificates/](https://kubernetes.io/docs/setup/best-practices/certificates/)

Now you can run kubeadm from your control plane directly

```bash
root@kind-control-plane:/# kubeadm token list
TOKEN                     TTL         EXPIRES                USAGES                   DESCRIPTION                                                EXTRA GROUPS
abcdef.0123456789abcdef   21h         2021-12-31T15:37:52Z   authentication,signing   <none>                                                     system:bootstrappers:kubeadm:default-node-token
```
Show the `discovery-token-ca-cert-hash` needed by nodes to connect to the control plane

```bash
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | \
   openssl dgst -sha256 -hex | sed 's/^.* //'
```

For example to manualy join a new node not using `Kind` you could use kubeadm from another Node as follows with the `token` and `discovery-token-ca-cert-hash`:

```bash
kubeadm join 192.168.122.185:6443 --token af03su.c1zkbiquknohhth6 \
	--discovery-token-ca-cert-hash sha256:684f1661b9c8d784b16622212b93bc270263639864837004f127abf0c2c44a3f
```
### Copy control plane kubernetes `admin.conf` (not needed with kind)

To connect clients to your control plane its good to know where the, cluster `certificate-authority-data`, user `client-certificate-data` and `client-key-data` is stored. In this case for user kubernetes-admin you can view the `admin.conf` and copy these details for connecting with kubectl or other tools once you have this information stored in that devices `~/.kube/config` or whereever its reaching out for that information to authenticate. Kind seems to connect without this file and folder but its likely stored somewhere I have not found yet?

```bash
cat /etc/kubernetes/admin.conf
```

# Workloads

* [https://kubernetes.io/docs/concepts/workloads/](https://kubernetes.io/docs/concepts/workloads/)

## Deployments

* [https://kubernetes.io/docs/concepts/workloads/controllers/deployment/](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)

Grab a pre-created deployment image for practice or use:

```bash
sudo kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
```
Before viewing Pods you must connect to the proxy in another terminal

```bash
sudo kubectl proxy
```

Checking out what deployments are running.

```bash
[trentonknight@archMac ~]$ sudo kubectl get deployments
[sudo] password for trentonknight:
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   1/1     1            1           7h21m
```

View Pods created for this Deployment

```bash
sudo kubectl get pods
```
View full details of same Pod created.

```bash
[trentonknight@archMac Kind]$ sudo kubectl describe pods
Name:         kubernetes-bootcamp-57978f5f5d-z7wg6
Namespace:    default
Priority:     0
Node:         kind-worker/172.18.0.4
Start Time:   Thu, 30 Dec 2021 11:19:18 -0500
Labels:       app=kubernetes-bootcamp
              pod-template-hash=57978f5f5d
Annotations:  <none>
Status:       Running
IP:           10.244.1.2
IPs:
  IP:           10.244.1.2
Controlled By:  ReplicaSet/kubernetes-bootcamp-57978f5f5d
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://fd98e2e61626e3bad38e45affc9adc53d0df4c094ad4c192d8d1f61393daa6ba
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v1
    Image ID:       gcr.io/google-samples/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Thu, 30 Dec 2021 11:19:35 -0500
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-qmlvd (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  kube-api-access-qmlvd:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  9m51s  default-scheduler  Successfully assigned default/kubernetes-bootcamp-57978f5f5d-z7wg6 to kind-worker
  Normal  Pulling    9m51s  kubelet            Pulling image "gcr.io/google-samples/kubernetes-bootcamp:v1"
  Normal  Pulled     9m37s  kubelet            Successfully pulled image "gcr.io/google-samples/kubernetes-bootcamp:v1" in 13.065273032s
  Normal  Created    9m34s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    9m34s  kubelet            Started container kubernetes-bootcamp
[trentonknight@archMac Kind]$ sudo kubectl logs kubernetes-bootcamp-57978f5f5d-z7wg6
[sudo] password for trentonknight:
Kubernetes Bootcamp App Started At: 2021-12-30T16:19:35.955Z | Running On:  kubernetes-bootcamp-57978f5f5d-z7wg6
```
We can [execute commands directly](https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/explore-interactive/) on the container once the Pod is up and running. For this, we use the exec command and use the name of the Pod as a parameter. Let’s list the environment variables:

```bash
sudo kubectl exec kubernetes-bootcamp-57978f5f5d-z7wg6 -- env
```
Start a bash session in the Pod similar to a Linux container locally.

```bash
sudo kubectl exec -ti kubernetes-bootcamp-57978f5f5d-z7wg6 -- bash
```
View the NodeJS app:

```bash
cat server.js
```
Check if the application is up and running

```bash
curl localhost:8080
```
# Ingress Controllers

* [https://kubernetes.github.io/ingress-nginx/deploy/](https://kubernetes.github.io/ingress-nginx/deploy/)

Use Ingress-Nginx YAML manifest to install pod

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.0/deploy/static/provider/cloud/deploy.yaml
```
Verify Install

```bash
kubectl get pods --namespace=ingress-nginx
```


# Expose Services for external access to applications

* [https://kubernetes.io/docs/tutorials/stateless-application/expose-external-ip-address/](https://kubernetes.io/docs/tutorials/stateless-application/expose-external-ip-address/)

First verify pods and services

```bash 
sudo kubectl get pods
sudo kubectl get services
``` 
Create a new service

```bash
sudo kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
```

View all aspects of the newly created service.

```bash
sudo kubectl describe services/kubernetes-bootcamp
```

# kubernetes-dashboard

* [https://github.com/kubernetes/dashboard](https://github.com/kubernetes/dashboard)

* [https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md)

* [https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)


You may get better results running this from the control panel directly? Results were as follows:

```bash
root@kind-control-plane:/# kubectl describe secrets
Name:         default-token-rsbl5
Namespace:    default
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: default
              kubernetes.io/service-account.uid: 80f40490-4ae5-470b-b402-af6805136351

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1066 bytes
namespace:  7 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6Ii1XOVBjMTEtdVQzYUNaTlhtaVNQcHVHc0FpV1dLSlZaS1Z2dkhBd2gwOFkifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQtdG9rZW4tcnNibDUiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjgwZjQwNDkwLTRhZTUtNDcwYi1iNDAyLWFmNjgwNTEzNjM1MSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ.UuCzzX5P3LW3ZJFFyHj4mUindLOd6UTTGlJYZ4SGnNr6aMAKtCh8WJD-xhDeGqoAeWmPe4uXU7d5IbvRR29NK0Crw3DYa16igEzfa1kVji1agF2PlCSDdvDEVLgUMUQOjiME27jA_TcMnO2DVDcOqKxPa75ZSMQBrlvMFPe7ow9SXpZX-fF34mpqT_f_T5WpvxvHFlCUebD9E9cgjVqPqNgeb6IvgM8TiK8yD0o2VyiOj1LkdMZ3AoChLQhrT1sy-4MGof3j1i-tCCSzlEVNCbLUKajt0mYe1wbe8H_cKSqQOAmIOyo9106s9fXLJm8K4W-KVtnADDa6mEUrK6J2_g
```

Run in second terminal where you are attempting access:

```bash
kubectl proxy
```
The access the dashboard at [http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/).

Use the `service-account-token` shown above. 

# LoadBalancers

* [https://metallb.universe.tf/](https://metallb.universe.tf/)
* [https://kind.sigs.k8s.io/docs/user/loadbalancer/](https://kind.sigs.k8s.io/docs/user/loadbalancer/)

## Metallb LoadBalancer for on Metal installs

Follow the Kind [tutorial](https://kind.sigs.k8s.io/docs/user/loadbalancer/) to install the Metallb Loadbalancer. This seems cleaner than doing a port forward and provides better training.

Example frontend service shown below using `type: LoadBalancer`.

```bash
# SOURCE: https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook
apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  # if your cluster supports it, uncomment the following to automatically create
  # an external load-balanced IP for the frontend service.
  # type: LoadBalancer
  type: LoadBalancer
  ports:
    # the port that this service should serve on
  - port: 80
  selector:
    app: guestbook
    tier: frontend
```

# Kubernetes API 

* [https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.23/](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.23/)
* [https://blog.tilt.dev/2021/03/18/kubernetes-is-so-simple.html](https://blog.tilt.dev/2021/03/18/kubernetes-is-so-simple.html)
* [https://github.com/kubernetes/api](https://github.com/kubernetes/api)
* [https://curl.se/](https://curl.se/)
* [https://everything.curl.dev/http](https://everything.curl.dev/http)
* [https://stedolan.github.io/jq/](https://stedolan.github.io/jq/)

List all supported [resource types](https://kubernetes.io/docs/reference/kubectl/cheatsheet/) along with their shortnames, API group, whether they are namespaced, and Kind:

```bash
kubectl api-resources | sort
```

The Kubernetes API has more hierarchy than /proc. It’s split into folders by version and namespace and resource type. The API path format looks like: `/api/[version]/namespaces/[namespace]/[resource]/[name]`

Use jq to list iterate objects.

```bash
[trentonknight@archMac REDIS]$ curl -s http://localhost:8001/api/v1/namespaces/ | jq '.items[].metadata.name'
"default"
"kube-node-lease"
"kube-public"
"kube-system"
"local-path-storage"
"metallb-system"
```
Use curl to view 

```bash
curl -s http://localhost:8001/api/v1/namespaces/kube-system/pods | head -n 20
```
To find the following `status` of the `kube-apiserver-kind-control-plane` as shown below you can narrow it down using `jq`

```bash
"status": {
    "phase": "Running",
    "conditions": [
      {
        "type": "Initialized",
        "status": "True",
        "lastProbeTime": null,
        "lastTransitionTime": "2022-01-01T03:02:48Z"
      },
      {
        "type": "Ready",
        "status": "True",
        "lastProbeTime": null,
        "lastTransitionTime": "2022-01-01T03:02:50Z"
      },
```

Using jq to find specifics

```bash
curl -s http://localhost:8001/api/v1/namespaces/kube-system/pods/kube-apiserver-kind-control-plane | jq '.status.phase'
```

# YAML files

* [https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/](https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/)
* [https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/](https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/)

In the .yaml file for the Kubernetes object you want to create, you'll need to set values for the following fields:

! apiVersion - Which version of the Kubernetes API you're using to create this object
! kind - What kind of object you want to create
! metadata - Data that helps uniquely identify the object, including a name string, UID, and optional namespace
! spec - What state you desire for the object

Here is an example deployment showing those values:

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2 # tells deployment to run 2 pods matching the template
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

Creating with kubectl:

```bash
kubectl apply -f https://k8s.io/examples/application/deployment.yaml --record
```
Or if the file has been edited locally

```bash
kubectl apply -f deployment.yaml --record
```


# Searching Kubernetes

To understand how to search for Kubernetes Objects review: [https://kubernetes.io/docs/concepts/overview/working-with-objects/](https://kubernetes.io/docs/concepts/overview/working-with-objects/) and specifically [https://kubernetes.io/docs/concepts/overview/working-with-objects/field-selectors/](https://kubernetes.io/docs/concepts/overview/working-with-objects/field-selectors/).

# Deleting Pod Issues

* [https://kubernetes.io/blog/2021/05/14/using-finalizers-to-control-deletion/](https://kubernetes.io/blog/2021/05/14/using-finalizers-to-control-deletion/)




