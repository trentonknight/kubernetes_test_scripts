# Fedora CoreOS

Fedora CoreOS allows you to create Kubernetes Nodes with a Immutable file structure. The Kubernetes Control Plane should not use Fedora CoreOS as installing needed tools will likely become impossible in some cases. 

# Ignition with qcow2 image
* [https://docs.fedoraproject.org/en-US/fedora-coreos/tutorial-autologin/](https://docs.fedoraproject.org/en-US/fedora-coreos/tutorial-autologin/)
* [https://github.com/coreos/butane/blob/main/docs/getting-started.md](https://github.com/coreos/butane/blob/main/docs/getting-started.md)
* [https://coreos.github.io/butane/](https://coreos.github.io/butane/)
* [https://docs.fedoraproject.org/en-US/fedora-coreos/](https://docs.fedoraproject.org/en-US/fedora-coreos/)

# Openshift, OKD links
* [https://docs.okd.io/latest/installing/installing_bare_metal/installing-bare-metal.html#creating-machines-bare-metal](https://docs.okd.io/latest/installing/installing_bare_metal/installing-bare-metal.html#creating-machines-bare-metal)
* [https://docs.okd.io/latest/architecture/architecture-rhcos.html](https://docs.okd.io/latest/architecture/architecture-rhcos.html)


# Generating a password hash for YAML file
Example from Flatcar Container Linux of creating a proper password hash using multiple proven tools: 
```bash
# On Debian/Ubuntu (via the package "whois")
mkpasswd --method=SHA-512 --rounds=4096

# OpenSSL (note: this will only make md5crypt.  While better than plantext it should not be considered fully secure)
openssl passwd -1

# Python
python -c "import crypt,random,string; print(crypt.crypt(input('clear-text password: '), '\$6\$' + ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(16)])))"

# Perl (change password and salt values)
perl -e 'print crypt("password","\$6\$SALT\$") . "\n"'
```
# Networking types
* [https://www.virtualbox.org/manual/ch06.html](https://www.virtualbox.org/manual/ch06.html)
* [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/configuring-virtual-machine-network-connections_configuring-and-managing-virtualization#virtual-networking-network-address-translation_types-of-virtual-machine-network-connections](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/configuring-virtual-machine-network-connections_configuring-and-managing-virtualization#virtual-networking-network-address-translation_types-of-virtual-machine-network-connections)

# YAML file
Create new YAML file such as your_config.fcc or your_config.yaml include public id_rsa.pub
```yaml
variant: fcos
version: 1.1.0
passwd:
  users:
    - name: core
      groups:
        - docker
        - wheel
        - sudo
      password_hash: XXXXXXX
      ssh_authorized_keys:
        - ssh-rsa XXXXXXXX= dude@dudescomputer
storage:
  files:
    - path: /etc/sysctl.d/20-silence-audit.conf
      contents:
        inline: |
          kernel.printk=4
    - path: /etc/hostname
      mode: 420
      contents:
        source: "data:,fcos"
    - path: /etc/NetworkManager/system-connections/enp1s0.nmconnection
      mode: 0600
      overwrite: true
      contents:
        inline: |
          [connection]
          type=ethernet
          id=network1
          interface-name=ens3

          [ipv4]
          method=manual
          addresses=192.168.99.100/24
          gateway=192.168.99.1
          dns=192.168.99.1;8.8.8.8
```
## Butane and Ignition for CoreOS build
Compiling new YAML based butane file to a Ignition file to be hosted using `python -m http.server` or similar or if being using in a virtual machine with `virt-manager` placed in a folder for libvirt use.
### Butane
* [https://coreos.github.io/butane/](https://coreos.github.io/butane/)

```bash
podman run -i --rm quay.io/coreos/butane:release --pretty --strict < your_config.bu > transpiled_config.ign
```

### Ignition
* [https://github.com/coreos/ignition](https://github.com/coreos/ignition)
Verify any directly modified ignition files. Only needed if you do that without using butane.

```bash
docker run --pull=always --rm -i quay.io/coreos/ignition-validate:release - < transpiled_config.ign
```

### Fedora-CoreOS qcow2 for Nodes
Pull the latest Fedora CoreOS using podman to run coreos-installer and place it in `/var/lib/libvirt/images`.

* [https://docs.fedoraproject.org/en-US/fedora-coreos/getting-started/](https://docs.fedoraproject.org/en-US/fedora-coreos/getting-started/)
### Coreos-installer with containers
```bash
sudo docker run \
--rm \
--tty \
--interactive \
--security-opt label=disable \
--volume ~/Development/coreos/:/pwd \
--workdir /pwd \
quay.io/coreos/coreos-installer:release \
--help
```

#### View Streams available:
Remove `--pull=always` after first run.
```bash
sudo docker run --pull=always \
--rm \
--tty \
--interactive \
--security-opt label=disable \
--volume ~/Development/coreos/:/pwd \
--workdir /pwd \
quay.io/coreos/coreos-installer:release \
list-stream
```
#### Fedora-CoreOS without coreos-installer
If coreos-installer is not working simply download Fedora-CoreOS from getfedora.org at this URL:
* [https://getfedora.org/coreos/download?tab=metal_virtualized&stream=stable](https://getfedora.org/coreos/download?tab=metal_virtualized&stream=stable)

Once the image is downloaded you may need to decompress the file with xz as follows:
```bash
xz --decompress foo.xz
```
# Network Install for Hardware.
Run a http or https server wityhin that same directory of the ignition file. For example:

```bash
python3 -m http.server
```
From machine booted from the latest FCOS ISO. 

```bash
sudo coreos-installer install /dev/sda --ignition-url http://<host machine ip>:8000/alpha.ign --insecure-ignition
```
# Building FCOS with libvirt 
## Ensure permissions are set for KVM and Qemu

```bash
sudo adduser $USER libvirt
sudo adduser $USER kvm
groups
id $USER

```
Ensure the PATH to both are cleared for both libvirt and qemu. For example make sure permissions to the ignition file `PATH` are set as follows:

```bash
sudo chmod a+x [path_to_volume_pool]\my_sweet_coreos_file.ign
```

Verify AppArmor or Selinux permits Qemu run. One exmaple found here:

* [https://unix.stackexchange.com/questions/578086/virt-install-error-cant-load-ignition-file](https://unix.stackexchange.com/questions/578086/virt-install-error-cant-load-ignition-file)

[Set](https://github.com/jedi4ever/veewee/issues/996) `security_driver = "none"` in `/etc/libvirt/qemu.conf` and restart `systemctl restart libvirtd`.

Worst case senario if using qemu or libvirt is blocked or fails temporarily disable selinux or apparmor to verify a solution then configure accordingly and re-enable checking after each change.
```bash
sudo systemctl disable apparmor
```

## Example of pulling qcow2.xz for new FCOS build
Set stream to STABLE before running command.
```bash
export STREAM="stable"
```
Examples 
```bash
docker run --pull=always \
--rm \
-v $HOME/.local/share/libvirt/images/:/data -w /data \
quay.io/coreos/coreos-installer:release \
download -s "${STREAM}" -p qemu -f qcow2.xz --decompress
```
In directory for running `virt-installer` as `sudo` 
```bash
docker run --pull=always \
--rm \
-v $HOME/.local/share/libvirt/images/:/data -w /data \
quay.io/coreos/coreos-installer:release \
download -s "${STREAM}" -p qemu -f qcow2.xz --decompress
```

```bash
sudo docker run --pull=always \
--rm \
--tty \
--interactive \
--security-opt label=disable \
--volume ~/Development/coreos/:/pwd \
--workdir /pwd \
quay.io/coreos/coreos-installer:release \
download -p qemu -f qcow2.xz --decompress
```
## Virt-Install for Nodes on Fedora CoreOS

* Move qcow2 file to `/var/lib/libvirt/images/`
* Move butane output ignition files to `/var/lib/libvirt/butane/`

Verfiy all PATH, permissions, groups and net-list, shown below, before attempting to virtualize using virt. Use sudo so at to have access to network hardware and avoid creating `User Mode` machines. `User Mode` machines will be isolated from each other but can connect to the outside network. 

```bash
sudo virt-install --name=newVM \
        --vcpus=2 \
        --ram=2048 \
        --os-variant=fedora6 \
        --import \
        --network=default \
        --graphics=none \
        --disk=size=20,backing_store=/var/lib/libvirt/images/fedora-coreos-34.20210904.3.0-qemu.x86_64.qcow2 \
        --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=/var/lib/libvirt/butane/newIgnition.ign"
```
## Verify with Virsh tool
Check with Virt-Manager. Check different Hypervisors using File -> Add Connection -> Such as QEMU/KVM User session. 
Verify virt machine later
```bash
sudo virsh list --all
```
Get Hostname of Fedora CoreOS
```bash
sudo virsh domifaddr fcos-test-01
```
Alernative method
```bash
sudo virsh net-list
sudo virsh net-info default
virsh net-start default
```
To start and stop domains
```bash
sudo virsh start fcos-test-01
sudo virsh shutdown fcos-test-01

sudo virsh destroy fcos-test-01
sudo virsh reboot fcos-test-01
```
Delete a domain completely

```bash
sudo virsh undefine fcos-test-01
``` 

Cosole to connect to domain directly
```bash
sudo virsh console fcos-test-01
sudo virsh ttyconsole fcos-test-01
```
SSH into Fedora CoreOS with ip address discovered using virsh
```bash
ssh core@X.X.X.X
```
### Issues with required bridged network and other troubleshooting

* [https://www.tecmint.com/create-network-bridge-in-ubuntu/](https://www.tecmint.com/create-network-bridge-in-ubuntu/)
* [https://wiki.libvirt.org/page/VirtualNetworking](https://wiki.libvirt.org/page/VirtualNetworking)

#### Creating default bridge
* [https://jamielinux.com/docs/libvirt-networking-handbook/](https://jamielinux.com/docs/libvirt-networking-handbook/)

> NAT based network
>> This example is the so called "default" virtual network. It is provided and enabled out-of-the-box for all libvirt installations. This is a configuration that allows guest OS to get outbound connectivity regardless of whether the host uses ethernet, wireless, dialup, or VPN networking without requiring any specific admin configuration. In the absence of host networking, it at least allows guests to talk directly to each other. 

```xml
<network>
  <name>default</name>
  <bridge name="virbr0"/>
  <forward mode="nat"/>
  <ip address="192.168.122.1" netmask="255.255.255.0">
    <dhcp>
      <range start="192.168.122.2" end="192.168.122.254"/>
    </dhcp>
  </ip>
  <ip family="ipv6" address="2001:db8:ca2:2::1" prefix="64"/>
</network>
```
```bash
sudo virsh net-create bridge.xml
sudo virsh net-edit default
```
Verify new default bridge virbr0.
```bash
sudo ip a
```
### Removing bridges
Some commands for tearing down and rebuilding bridges. virt creates virbr0 and virbr0-nic. To remove and customize do the following:

```bash
sudo nmcli conn show --active
```
Check existing default bridge used by virsh
```bash
sudo virsh net-edit default
```
Check and delete existing bridges
```bash
brctl show
sudo ip link set virbr0 down
sudo ip link set virbr0-nic down
sudo brctl stp virbr0 off
sudo brctl stp virbr0 off
sudo brctl delbr virbr0
```
Remove bridges 
```bash
sudo ip link set virbr0 down
sudo ip link set virbr0-nic down
sudo ip link delete virbr0
sudo ip link set virbr0-nic down
```
Check active network remaining
```bash
sudo nmcli conn show --active
```
# Kubernetes on Fedora CoreOS or Flatcar Container Linux
* [https://www.codetab.org/post/kubernetes-cluster-qemu/](https://www.codetab.org/post/kubernetes-cluster-qemu/)
* [https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

# Building Control Plane on mutable cloud-init servers for Kubernetes

* [https://alt.fedoraproject.org/cloud/](https://alt.fedoraproject.org/cloud/)
* [https://cloudinit.readthedocs.io/en/latest/topics/examples.html](https://cloudinit.readthedocs.io/en/latest/topics/examples.html)
* [https://blog.wikichoon.com/2020/09/virt-install-cloud-init.html](https://blog.wikichoon.com/2020/09/virt-install-cloud-init.html)
* [https://github.com/virt-manager/virt-manager/blob/master/man/virt-install.rst#--cloud-init](https://github.com/virt-manager/virt-manager/blob/master/man/virt-install.rst#--cloud-init)
* [https://cloud.centos.org/centos/](https://cloud.centos.org/centos/)
* [https://cdimage.debian.org/cdimage/cloud/](https://cdimage.debian.org/cdimage/cloud/)

If you plan on doing repeat installs you may want to instead pull from a `qcow2` file you store in the `/var/lib/libvirt/images` folder for repated use. A couple examples below is using the Fedora Cloud Based image or CentOS 8.

## Debian 11

```bash
sudo virt-install --name=control \
--vcpus=2 \
--ram=2048 \
--os-variant=debian11 \
--import \
--network=default \
--graphics=none \
--disk=size=20,backing_store=/var/lib/libvirt/images/debian-11-generic-amd64-20210814-734.qcow2 \
--graphics vnc \
--cloud-init ssh-key=$HOME/.ssh/id_rsa.pub
```
## Fedora cloud-init image

```bash
sudo virt-install --name=control \
--vcpus=2 \
--ram=2048 \
--os-variant=fedora35 \
--import \
--network=default \
--graphics=none \
--disk=size=20,backing_store=/var/lib/libvirt/images/Fedora-Cloud-Base-35-1.2.x86_64.qcow2 \
--cloud-init ssh-key=$HOME/.ssh/id_rsa.pub
```
## Ubuntu cloud-init image

```bash
sudo virt-install --name=control \
--vcpus=2 \
--ram=2048 \
--os-variant=ubuntu18.04 \
--import \
--network=default \
--graphics=none \
--disk=size=20,backing_store=/var/lib/libvirt/images/bionic-server-cloudimg-amd64.img \
--graphics vnc \
--cloud-init ssh-key=$HOME/.ssh/id_rsa.pub
```
You should see something at the begining of launching Fedora Cloud based offering you a temporary default password:

```bash
WARNING  Defaulting to --cloud-init root-password-generate=yes,disable=yes

Starting install...
Password for first root login is: rPS5X0vZJA4yDJys
```
To pull the latest fedora image from the internet simple use:

```bash
osinfo-query os | grep cloud
```

```bash
sudo virt-install --name=control \
--vcpus=2 \
--ram=2048 \
--os-variant=fedora35 \
--import \
--network=default \
--graphics=none \
--install fedora35 \
--cloud-init ssh-key=$HOME/.ssh/id_rsa.pub
```
# Building Kubernetes Control Plane on Fedora CoreOS
* [https://gist.github.com/rkaramandi/44c7cea91501e735ea99e356e9ae7883](https://gist.github.com/rkaramandi/44c7cea91501e735ea99e356e9ae7883)
* [https://github.com/flannel-io/flannel](https://github.com/flannel-io/flannel)


## Bair bones Kubernetes setup

```bash
# Set SELinux in permissive mode (effectively disabling it)
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```
## iptables
> Make sure that the br_netfilter module is loaded. This can be done by running lsmod | grep br_netfilter. To load it explicitly call sudo modprobe br_netfilter.
> As a requirement for your Linux Node's iptables to correctly see bridged traffic, you should ensure net.bridge.bridge-nf-call-iptables is set to 1 in your sysctl config, e.g. 

```bash
vim /etc/sysctl.conf
```
Uncomment the following line:

```bash
net.ipv4.ip_forward=1
```
When first running kubeadm init, many of these might change. Try and match errors from init failures.

Install containerd and make sure to make the fllowing edits:

```bash
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

# Setup required sysctl params, these persist across reboots.
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

# Apply sysctl params without reboot
sudo sysctl --system
```

```bash
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
```
Double check network ports here.
* [https://kubernetes.io/docs/reference/ports-and-protocols/](https://kubernetes.io/docs/reference/ports-and-protocols/)

```bash
sudo lsof -nP -iTCP -sTCP:LISTEN
```
```bash
sudo netstat -tulnp
```
Check iptables current config
```bash
sudo iptables -L
```
## rpm-ostree install kubeadm and other tools
Add repository for rpm-ostree on nodes and dnf on the control plane

```bash
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```
On Nodes running Fedora CoreOS

```bash
sudo rpm-ostree update --bypass-driver
sudo rpm-ostree install kubelet kubeadm  
```
On the control plane running Fedora Server

```bash
sudo dnf -y install kubelet kubeadm kubectl
```

## hostnamectl
If hostname was not already setup via ignition use the following to make that change before reboot. For example:
```bash
sudo hostnamectl set-hostname control
```
Possibly needed for kubelet issue?
```bash
 sudo swapoff -a
```
Reboot FCOS are new layers are added then enable kubelet.
## Kubernetes cluster build and CNI VERY basic on Fedora Server being used as the control plane
Enable kubelet and docker or init will fail

```bash
sudo systemctl enable --now kubelet
sudo systemctl enable docker
sudo systemctl start kubelet
```

### CIDR

Let say you ip address in your VM is 192.168.122.123. Then use the following CIDR while initing your new cluster. This is for your CNI of which you install next.

```bash
sudo kubeadm init --pod-network-cidr=192.168.12
```
Example cluster config file for `sudo kubeadm init --config kubeadm-config.yaml` install instead.

```bash
cat << EOF > kubeadm-config.yaml
apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
nodeRegistration:
  kubeletExtraArgs:
    volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"
localAPIEndpoint:
  advertiseAddress: "192.168.99.101"
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  podSubnet: "192.168.0.0/16"
controllerManager:
  extraArgs:
    flex-volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"
apiServer:
  extraArgs:
    advertise-address: 192.168.99.101

EOF
```

Verify cluster cidr
```bash
ps -ef | grep "cluster-cidr"
```
Reset cluster in the future. If something goes wrong or whatnot.
* [https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-reset/](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-reset/)

```bash
kubeadm reset 
```
> To start using your cluster, you need to run the following as a regular user:
Verify admin.conf actually exists and is copied to .kube/config or pod will be unable to authenticate and connect.

```bash
mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
### Add Container Network Interface (CNI) Flannel or Calico.

* [https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/)

#### Flannel

* [https://github.com/flannel-io](https://github.com/flannel-io)



#### Calico

* [https://docs.projectcalico.org/getting-started/kubernetes/](https://docs.projectcalico.org/getting-started/kubernetes/)

* [https://www.codetab.org/post/kubernetes-cluster-virtualbox/](https://www.codetab.org/post/kubernetes-cluster-virtualbox/)

```bash
kubectl apply -f calico.yaml

watch kubectl get pods --all-namespaces 
```

### View cluster 
```bash
kubectl version
kubectl cluster-info
kubectl get nodes
```
## Join nodes

### Check tokens first

After your cluster was built you saw the following:

```bash
kubeadm join 192.168.122.185:6443 --token af03su.c1zkbiquknohhth6 \
	--discovery-token-ca-cert-hash sha256:684f1661b9c8d784b16622212b93bc270263639864837004f127abf0c2c44a3f
```
However assuming you did not write it down or save it anywhere here is how you later verify tokens
```bash
kubeadm token list
```
If none exist you can create a new token.

```bash
kubeadm token create
```

> If you don't have the value of --discovery-token-ca-cert-hash, you can get it by running the following command chain on the control-plane node:

```bash
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | \
   openssl dgst -sha256 -hex | sed 's/^.* //'
```
### Join node

> The nodes are where your workloads (containers and Pods, etc) run. To add new nodes to your cluster do the following for each machine:
>> SSH to the machine
>> Become root (e.g. sudo su -)
>> Install a runtime if needed
>> Run the command that was output by kubeadm init. For example:

```bash
kubeadm join --token <token> <control-plane-host>:<control-plane-port> --discovery-token-ca-cert-hash sha256:<hash>
```
Alternativly, a much cleaner method from [Codetab](https://www.codetab.org/post/kubernetes-cluster-virtualbox/) uses the following method.

```bash
JOIN_TOKEN=<paste-token-from-master-here>
JOIN_CERT_HASH=<paste-cert-hash-from-master-here>
```
Now cat the following:

```bash
cat <<EOF > kubeadm-join.yaml
apiVersion: kubeadm.k8s.io/v1beta2
kind: JoinConfiguration
nodeRegistration:
  kubeletExtraArgs:
    volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"
discovery:
  bootstrapToken:
    apiServerEndpoint: 192.168.99.101:6443
    token: ${JOIN_TOKEN}
    caCertHashes:
    - sha256:${JOIN_CERT_HASH}
EOF
```
### verify whether variables are substituted properly 

```bash
cat kubeadm-join.yaml
```
##### Joined node result
What you should see once a Node connects.
```bash
[core@node ~]$ sudo kubeadm join --config kubeadm-join.yaml
[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.
```

# Kubernetes guides

* [https://kubectl.docs.kubernetes.io/](https://kubectl.docs.kubernetes.io/)

# Remote management
Retrieve config file from control plane which created the cluster you wish to reach. This command is from your local folder on what will be a client using kubectl or helm remotely. Run the following command in your home drive. This command is copying the existing config file from the server running kubernetes to your local computer. You then need to mirror the `.kube` diectory and include the config file which allows kubectl to authenticate and communicate with that servers cluster.

```bash
mkdir .kube
cd .kube
scp core@192.168.122.231:~/.kube/config .
```
## Install kubectl
* [https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
## Install HELM
* [https://helm.sh/docs/intro/install/](https://helm.sh/docs/intro/install/)



